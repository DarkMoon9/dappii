package uia.dappii.practica11;
import java.io.*;
import java.net.ServerSocket;
import java.net.Socket;

public class Main {
    public static void main(String[] args) throws IOException {
        int puerto = 8000;
        int c;
        ServerSocket miServidor = new ServerSocket(puerto);
        Socket sc;

        System.out.println("\nEsperando conexión...");
        sc = miServidor.accept();
        System.out.println("\nConexión aceptada...");

        InputStreamReader streamSocket = new InputStreamReader(sc.getInputStream());
        BufferedReader lectorSocket = new BufferedReader(streamSocket);
        PrintWriter escritorSocket = new PrintWriter(sc.getOutputStream(), true);

        String mensajeRecibido, mensajeEnviado;

        System.out.println("\nEsperando mensaje");
        mensajeRecibido = lectorSocket.readLine();

        System.out.println(mensajeRecibido);

        FileReader fr = new FileReader("D:\\Practica 11\\src\\main\\resources\\Práctica_4.html");
        StringBuilder contenido = new StringBuilder();

        while ((c = fr.read()) != -1){
            contenido.append((char) c);
        }

        System.out.println(contenido);
        mensajeEnviado = "HTTP/1.1 200 OK\n";

        mensajeEnviado += "Content-Type: text/html\n";
        mensajeEnviado += "Content-Length: " + contenido.length() + "\n\n";
        mensajeEnviado += contenido;

        escritorSocket.println(mensajeEnviado);
        System.out.println("\nMensaje a enviar: "+ mensajeEnviado);
        System.out.println("Enviando mensaje");
    }
}
