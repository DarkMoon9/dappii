package uia.dappii.cliente;
import uia.dappii.cliente.modelo.Persona;
import uia.dappii.cliente.service.PersonaService;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;

public class ClienteSocket {
    public static void main(String[] args) throws IOException {
        String host = "127.0.0.1";
        String nombre, paterno, materno, genero;
        int puerto = 8080, op;
        long id;
        boolean estatus;

        System.out.println("Conectando al Servidor...");
        Socket cliente = new Socket(host, puerto);

        System.out.println("Me conecte al servidor!!!");
        BufferedReader lectorSocket;
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        PrintWriter out;

        InputStreamReader in = new InputStreamReader(cliente.getInputStream());
        lectorSocket = new BufferedReader(in);
        out = new PrintWriter(cliente.getOutputStream(), true);

        String mensajeTransmitido, mensajeRecibido;

        do {
            op = menu();
            switch (op) {
                case 1:
                    mensajeTransmitido = "GET http://localhost:8080/";
                    out.println(mensajeTransmitido);
                    System.out.println("Enviando mensaje...");

                    mensajeRecibido = lectorSocket.readLine();
                    System.out.println(mensajeRecibido);
                    break;
                case 2:
                    System.out.print("\nDigita tu nombre: ");
                    nombre = br.readLine();

                    System.out.print("\nDigita tu A. Paterno: ");
                    paterno = br.readLine();

                    System.out.print("\nDigita tu A. Materno: ");
                    materno = br.readLine();

                    System.out.print("\nDigita tu género: ");
                    genero = br.readLine();
                    System.out.print("\nEstatus: ");
                    estatus = Boolean.parseBoolean(br.readLine());

                    Persona persona = new Persona(nombre, paterno, materno, genero, estatus);
                    PersonaService.postMethod(persona);
                    System.out.println(persona.toJson());

                    System.out.println("Enviando datos...");
                    break;
                case 3:
                    System.out.print("\nDigita tu id: ");
                    id = Long.parseLong(br.readLine());

                    System.out.println("Enviando ID...");

                    System.out.print("\nDigita tu nombre: ");
                    nombre = br.readLine();

                    System.out.print("\nDigita tu A. Paterno: ");
                    paterno = br.readLine();

                    System.out.print("\nDigita tu A. Materno: ");
                    materno = br.readLine();

                    System.out.print("\nDigita tu género: ");
                    genero = br.readLine();

                    System.out.print("\nDigita tu estatus: ");
                    estatus = Boolean.parseBoolean(br.readLine());

                    Persona persona1 = new Persona(id, nombre, paterno, materno, genero, estatus);
                    System.out.println(persona1.toJsonX());
                    PersonaService.putMethod(persona1);
                    System.out.println("Enviando datos...");
                    break;
                case 4:
                    System.out.print("\nDigita tu id: ");
                    id = Long.parseLong(br.readLine());
                    mensajeTransmitido = "DELETE http://localhost:8080/borrar/" + id;
                    out.println(mensajeTransmitido);
                    System.out.println("Enviando mensaje...");
                    break;
                case 5:
                    opcionSalir();
                    break;
                default:
                    opcionDefault();
                    break;
            }
        } while (op != 5);
    }

    public static int menu() throws IOException {
        int opcion;
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        System.out.println("\n1.- GET");
        System.out.println("2.- POST");
        System.out.println("3.- PUT");
        System.out.println("4.- DELETE");
        System.out.println("5.- Salir");
        System.out.print("\nIndique su opcion: ");
        opcion = Integer.parseInt(br.readLine());
        return opcion;
    }

    public static void opcionSalir(){
        System.out.println("Hasta luego!");
    }

    public static void opcionDefault(){
        System.out.println("Por favor, elije una opción valida...");
    }

}

