package uia.dappii.cliente.modelo;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class Persona {
    private Long idPersona;
    private String nombre;
    private String paterno;
    private String materno;
    private String genero;
    private Boolean estatus;

    public Persona(String nombre, String paterno, String materno, String genero, Boolean estatus){
        this.nombre = nombre;
        this.paterno = paterno;
        this.materno = materno;
        this.genero = genero;
        this.estatus = estatus;
    }

    public String toJson() {
        String cadenaJson = "{";
        cadenaJson += "\n\t\"nombre\":\""+ nombre +"\",";
        cadenaJson += "\n\t\"paterno\":\""+ paterno +"\",";
        cadenaJson += "\n\t\"materno\":\""+ materno +"\",";
        cadenaJson += "\n\t\"genero\":\""+ genero +"\",";
        cadenaJson += "\n\t\"estatus\":" + estatus;
        cadenaJson += "\n}";

        return cadenaJson;
    }

    public String toJsonX() {
        String cadenaJson = "{";
        cadenaJson += "\n\t\"idPersona\":\""+ idPersona +"\",";
        cadenaJson += "\n\t\"nombre\":\""+ nombre +"\",";
        cadenaJson += "\n\t\"paterno\":\""+ paterno +"\",";
        cadenaJson += "\n\t\"materno\":\""+ materno +"\",";
        cadenaJson += "\n\t\"genero\":\""+ genero +"\",";
        cadenaJson += "\n\t\"estatus\":" + estatus;
        cadenaJson += "\n}";

        return cadenaJson;
    }
}
