package uia.dappii.servidor.model;
import javax.persistence.*;
import lombok.Data;

@Entity
@Data
@Table(name = "persona", schema = "practica_14")
public class Persona {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long idPersona;

    @Column(name = "nombre")
    private String nombre;

    @Column(name = "paterno")
    private String paterno;

    @Column(name = "materno")
    private String materno;

    @Column(name = "genero")
    private String genero;

    @Column(name = "estatus")
    private Boolean estatus;
}
