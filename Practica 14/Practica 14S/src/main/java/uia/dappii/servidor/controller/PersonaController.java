package uia.dappii.servidor.controller;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import uia.dappii.servidor.model.Persona;
import uia.dappii.servidor.service.IPersonaService;
import java.util.List;

@RestController
@RequestMapping
public class PersonaController {

    @Autowired
    private IPersonaService personaService;

    @GetMapping("/")
    public List<Persona> homeView(){
        return personaService.getAllPersonas();
    }

    @PostMapping(value = "/", consumes = "application/json", produces = "application/json")
    public String guardarPersona(@RequestBody Persona persona){
        personaService.guardarPersona(persona);
        return "Se ha insertado con exito";
    }

    @PutMapping(value = "/", consumes = "application/json", produces = "application/json")
    public String actualizarPersona(@RequestBody Persona persona){
        personaService.guardarPersona(persona);
        return "Se ha actualizado";
    }

    @DeleteMapping("/borrar/{id}")
    public void borrarPersona(@PathVariable(value = "id") Long id){
       personaService.borrarPersonaById(id);
    }
}
