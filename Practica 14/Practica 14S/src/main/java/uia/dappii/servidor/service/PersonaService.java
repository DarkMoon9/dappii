package uia.dappii.servidor.service;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import uia.dappii.servidor.model.Persona;
import uia.dappii.servidor.repository.IPersonaRepository;
import java.util.List;
import java.util.Optional;

@Service
public class PersonaService implements IPersonaService {

    @Autowired
    IPersonaRepository personaRepository;

    @Override
    public List<Persona> getAllPersonas() {
        System.out.println("Retornando la lista...");
        return personaRepository.findAll();
    }

    @Override
    public void guardarPersona(Persona persona) {
        this.personaRepository.save(persona);
    }

    @Override
    public Persona getPersonaById(Long id) {
        Optional<Persona> optional = personaRepository.findById(id);
        Persona persona = null;
        if(optional.isPresent()){
            persona = optional.get();
        } else {
            throw new RuntimeException("La persona con el id: " + id + " no ha sido encontrado.");
        }
        return persona;
    }

    @Override
    public void borrarPersonaById(Long id) {
        this.personaRepository.deleteById(id);
    }


}
