package uia.dappii.servidor.service;

import uia.dappii.servidor.model.Persona;

import java.util.List;

public interface IPersonaService {
    List<Persona> getAllPersonas();
    void guardarPersona(Persona persona);
    Persona getPersonaById(Long id);
    void borrarPersonaById(Long id);

}
