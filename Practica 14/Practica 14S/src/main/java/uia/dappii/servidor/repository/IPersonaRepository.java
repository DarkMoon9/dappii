package uia.dappii.servidor.repository;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import uia.dappii.servidor.model.Persona;

@Repository
public interface IPersonaRepository extends JpaRepository<Persona, Long> {
}
