package uia.dappii.practica12.cliente;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;

public class SocketCliente {
    public static void main(String[] args) throws IOException {
        String host = "127.0.0.1";
        int puerto = 8000, op;
        Integer id, edad;
        Boolean estatus;
        String nombre, paterno, materno, genero;

        System.out.println("Conectando al Servidor...");
        Socket cliente = new Socket(host, puerto);

        System.out.println("Me conecte al servidor!!!");
        BufferedReader lectorSocket;
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        PrintWriter out;

        InputStreamReader in = new InputStreamReader(cliente.getInputStream());
        lectorSocket = new BufferedReader(in);
        out = new PrintWriter(cliente.getOutputStream(), true);

        String mensajeTransmitido, mensajeRecibido;

        do {
            op = menu();
            switch (op) {
                case 1:
                    mensajeTransmitido = "GET";
                    out.println(mensajeTransmitido);
                    System.out.println("Enviando mensaje...");

                    mensajeRecibido = lectorSocket.readLine();
                    System.out.println(mensajeRecibido);

                    mensajeRecibido = lectorSocket.readLine();
                    System.out.println(mensajeRecibido);

                    mensajeRecibido = lectorSocket.readLine();
                    System.out.println(mensajeRecibido);
                    break;
                case 2:
                    mensajeTransmitido = "POST";
                    out.println(mensajeTransmitido);
                    System.out.println("Enviando mensaje...");

                    System.out.print("\nDigita tu nombre: ");
                    nombre = br.readLine();
                    out.println(nombre);

                    System.out.print("\nDigita tu A. Paterno: ");
                    paterno = br.readLine();
                    out.println(paterno);

                    System.out.print("\nDigita tu A. Materno: ");
                    materno = br.readLine();
                    out.println(materno);

                    System.out.print("\nDigita tu edad: ");
                    edad = Integer.valueOf(br.readLine());
                    out.println(edad);

                    System.out.print("\nDigita tu género: ");
                    genero = br.readLine();
                    out.println(genero);

                    System.out.print("\nEstatus: ");
                    estatus = Boolean.valueOf(br.readLine());
                    out.println(estatus);

                    System.out.println("Enviando datos...");
                    break;
                case 3:
                    mensajeTransmitido = "PUT";
                    out.println(mensajeTransmitido);
                    System.out.println("Enviando mensaje...");

                    System.out.print("\nDigita tu id: ");
                    id = Integer.parseInt(br.readLine());
                    out.println(id);

                    System.out.println("Enviando ID...");

                    System.out.print("\nDigita tu nombre: ");
                    nombre = br.readLine();
                    out.println(nombre);

                    System.out.print("\nDigita tu A. Paterno: ");
                    paterno = br.readLine();
                    out.println(paterno);

                    System.out.print("\nDigita tu A. Materno: ");
                    materno = br.readLine();
                    out.println(materno);

                    System.out.print("\nDigita tu edad: ");
                    edad = Integer.valueOf(br.readLine());
                    out.println(edad);

                    System.out.print("\nDigita tu género: ");
                    genero = br.readLine();
                    out.println(genero);

                    System.out.println("Enviando datos...");
                    break;
                case 4:
                    mensajeTransmitido = "DELETE";
                    out.println(mensajeTransmitido);
                    System.out.println("Enviando mensaje...");

                    System.out.print("\nDigita tu id: ");
                    id = Integer.parseInt(br.readLine());
                    out.println(id);
                    System.out.println("Enviando mensaje...");
                    break;
                case 5:
                    opcionSalir();
                    break;
                default:
                    opcionDefault();
                    break;
            }
        } while (op != 5);
    }

    public static int menu() throws IOException {
        int opcion;
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        System.out.println("\n1.- GET");
        System.out.println("2.- POST");
        System.out.println("3.- PUT");
        System.out.println("4.- DELETE");
        System.out.println("5.- Salir");
        System.out.print("\nIndique su opcion: ");
        opcion = Integer.parseInt(br.readLine());
        return opcion;
    }

    public static void opcionSalir(){
        System.out.println("Hasta luego!");
    }

    public static void opcionDefault(){
        System.out.println("Por favor, elije una opción valida...");
    }
}

