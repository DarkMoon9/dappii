package uia.dappii.practica12.modelo;
import java.util.ArrayList;

public class GestorPersonas {
    private ArrayList<Persona> personas = new ArrayList<>();

    public GestorPersonas(){}

    public ArrayList<Persona> getPersonas() {
        return personas;
    }

    public void setPersonas(ArrayList<Persona> personas) {
        this.personas = personas;
    }
}
