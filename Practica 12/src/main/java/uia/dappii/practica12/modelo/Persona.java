package uia.dappii.practica12.modelo;
import java.util.Objects;

public class Persona {
    private Integer idPersona;
    private String nombre;
    private String paterno;
    private String materno;
    private Integer edad;
    private String genero;
    private Boolean estatus;

    public Persona(Integer idPersona, String nombre, String paterno, String materno, Integer edad, String genero, Boolean estatus) {
        this.idPersona = idPersona;
        this.nombre = nombre;
        this.paterno = paterno;
        this.materno = materno;
        this.edad = edad;
        this.genero = genero;
        this.estatus = estatus;
    }

    public Persona(){}

    public void setIdPersona(Integer idPersona) {
        this.idPersona = idPersona;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getPaterno() {
        return paterno;
    }

    public void setPaterno(String paterno) {
        this.paterno = paterno;
    }

    public String getMaterno() {
        return materno;
    }

    public void setMaterno(String materno) {
        this.materno = materno;
    }

    public Integer getEdad() {
        return edad;
    }

    public void setEdad(Integer edad) {
        this.edad = edad;
    }

    public String getGenero() {
        return genero;
    }

    public void setGenero(String genero) {
        this.genero = genero;
    }

    public Boolean getEstatus() {
        return estatus;
    }

    public void setEstatus(Boolean estatus) {
        this.estatus = estatus;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Persona that = (Persona) o;
        return Objects.equals(idPersona, that.idPersona) && Objects.equals(nombre, that.nombre) && Objects.equals(paterno, that.paterno) && Objects.equals(materno, that.materno) && Objects.equals(edad, that.edad) && Objects.equals(genero, that.genero) && Objects.equals(estatus, that.estatus);
    }

    @Override
    public int hashCode() {
        return Objects.hash(idPersona, nombre, paterno, materno, edad, genero, estatus);
    }

    @Override
    public String toString() {
        return "Persona{" +
                "idPersona=" + idPersona +
                ", nombre='" + nombre + '\'' +
                ", paterno='" + paterno + '\'' +
                ", materno='" + materno + '\'' +
                ", edad=" + edad +
                ", genero='" + genero + '\'' +
                ", estatus=" + estatus +
                '}';
    }
}
