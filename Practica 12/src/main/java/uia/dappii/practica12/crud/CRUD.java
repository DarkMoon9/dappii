package uia.dappii.practica12.crud;
import uia.dappii.practica12.modelo.Persona;
import java.sql.*;
import java.util.ArrayList;

public class CRUD {
    public ArrayList<Persona> select (Connection conn) throws SQLException {
        ArrayList<Persona> personas = new ArrayList<>();
        String sql = "SELECT id_persona, nombre, paterno, materno, edad, genero, estatus FROM persona";
        Statement stmt = conn.createStatement();
        ResultSet rs = stmt.executeQuery(sql);

        while (rs.next()){
            Persona persona = new Persona();
            persona.setIdPersona(rs.getInt(1));
            persona.setNombre(rs.getString(2));
            persona.setPaterno(rs.getString(3));
            persona.setMaterno(rs.getString(4));
            persona.setEdad(rs.getInt(5));
            persona.setGenero(rs.getString(6));
            persona.setEstatus(rs.getBoolean(7));
            personas.add(persona);
            persona = null;
        }
        stmt.close();
        rs.close();
        return personas;
    }

    public int insert(Connection conn, Persona persona) throws SQLException {
        String sql = "INSERT INTO persona (id_persona, nombre, paterno, materno, edad, genero, estatus)" +
                "     VALUES (null,?,?,?,?,?,?)";
        PreparedStatement pstmt = conn.prepareStatement(sql);

        pstmt.setString(1, persona.getNombre());
        pstmt.setString(2, persona.getPaterno());
        pstmt.setString(3, persona.getMaterno());
        pstmt.setInt(4, persona.getEdad());
        pstmt.setString(5, persona.getGenero());
        pstmt.setBoolean(6, persona.getEstatus());
        int i = pstmt.executeUpdate();
        pstmt.close();
        System.out.println("\n Se ha insertado el registro...");
        return 0;
    }

    public int update(Connection conn, Persona persona, Integer id) throws SQLException {
        String sql = "UPDATE persona SET nombre = ?, paterno = ?, materno = ?, edad = ?, genero = ? WHERE id_persona = " + "'" + id + "'";
        PreparedStatement pstmt = conn.prepareStatement(sql);
        pstmt.setString(1, persona.getNombre());
        pstmt.setString(2, persona.getPaterno());
        pstmt.setString(3, persona.getMaterno());
        pstmt.setString(4, String.valueOf(persona.getEdad()));
        pstmt.setString(5, persona.getGenero());
        int i = pstmt.executeUpdate();
        pstmt.close();
        System.out.println("Se ha actualizado correctamente...");
        return i;
    }

    public void delete(Connection conn, Integer id) throws SQLException {
        Statement stmt = conn.createStatement();
        String sql = "DELETE FROM persona WHERE id_persona = " + "'" + id + "'";
        stmt.executeUpdate(sql);
        stmt.close();
        System.out.println("Se ha eliminado el registro");
    }
}
