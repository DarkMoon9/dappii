package uia.dappii.practica12.servidor;
import uia.dappii.practica12.conexion.Conexion;
import uia.dappii.practica12.crud.CRUD;
import uia.dappii.practica12.modelo.GestorPersonas;
import uia.dappii.practica12.modelo.Persona;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;
import java.sql.Connection;
import java.sql.SQLException;

public class SocketServidor {
    public static void main(String[] args) throws IOException, SQLException {
        int puerto = 8000;
        Conexion conexion = new Conexion();
        Connection conn = conexion.conectar();
        GestorPersonas gestorPersonas = new GestorPersonas();
        CRUD crud = new CRUD();
        Integer id, edad;
        Boolean estatus;
        String nombre, paterno, materno, genero;

        ServerSocket miServidor = new ServerSocket(puerto);
        Socket miCliente;

        System.out.println("Esperando Conexion...");

        miCliente = miServidor.accept();

        System.out.println("Recibi Conexion");

        BufferedReader lectorSocket;
        PrintWriter out;

        InputStreamReader in = new InputStreamReader(miCliente.getInputStream());
        lectorSocket = new BufferedReader(in);

        out = new PrintWriter(miCliente.getOutputStream(), true);

        String mensajeRecibido;

        System.out.println("Esperando mensaje....");

        mensajeRecibido = lectorSocket.readLine();

        switch (mensajeRecibido) {
            case "GET":
                System.out.println("\nEstoy en el GET");
                out.println("\nObteniendo información de la base de datos...");
                gestorPersonas.setPersonas(crud.select(conn));
                System.out.println(gestorPersonas.getPersonas());
                out.println(gestorPersonas.getPersonas());
                break;
            case "POST":
                System.out.println("Estoy en en POST");

                System.out.print("\nNombre: ");
                nombre = lectorSocket.readLine();
                System.out.print(nombre);

                System.out.print("\nA. Paterno: ");
                paterno = lectorSocket.readLine();
                System.out.print(paterno);

                System.out.print("\nA. Materno: ");
                materno = lectorSocket.readLine();;
                System.out.print(materno);

                System.out.print("\nEdad: ");
                edad = Integer.valueOf(lectorSocket.readLine());
                System.out.print(edad);

                System.out.print("\nGénero: ");
                genero = lectorSocket.readLine();
                System.out.print(genero);

                System.out.print("\nEstatus: ");
                estatus = Boolean.parseBoolean(lectorSocket.readLine());
                System.out.print(estatus);

                System.out.println("\nDatos recibidos correctamente...");
                Persona persona = new Persona(null, nombre, paterno, materno, edad, genero, estatus);
                crud.insert(conn, persona);
                break;
            case "PUT":
                System.out.println("\nEstoy en PUT");
                Persona update = new Persona();
                System.out.print("\nID: ");
                id = Integer.valueOf(lectorSocket.readLine());
                System.out.println(id);
                update.setIdPersona(id);

                System.out.println("ID recibido correctamente..");

                System.out.print("\nNombre: ");
                nombre = lectorSocket.readLine();
                System.out.print(nombre);
                update.setNombre(nombre);

                System.out.print("\nA. Paterno: ");
                paterno = lectorSocket.readLine();
                System.out.print(paterno);
                update.setPaterno(paterno);

                System.out.print("\nA. Materno: ");
                materno = lectorSocket.readLine();;
                System.out.print(materno);
                update.setMaterno(materno);

                System.out.print("\nEdad: ");
                edad = Integer.valueOf(lectorSocket.readLine());
                System.out.print(edad);
                update.setEdad(edad);

                System.out.print("\nGénero: ");
                genero = lectorSocket.readLine();
                System.out.print(genero);
                update.setGenero(genero);

                System.out.println("\nDatos recibidos correctamente...");
                crud.update(conn, update, id);
                break;
            case "DELETE":
                System.out.println("Estoy en delete...");
                Persona delete = new Persona();
                System.out.print("\nID: ");
                id = Integer.valueOf(lectorSocket.readLine());
                System.out.println(id);

                System.out.println("ID recibido correctamente...");

                delete.setIdPersona(id);
                crud.delete(conn, id);
                break;
            default:
                System.out.println("Opción no valida.");
                break;
        }
        System.out.println("Opcion: " + mensajeRecibido);
        conexion.desconectar(conn);
    }
}
