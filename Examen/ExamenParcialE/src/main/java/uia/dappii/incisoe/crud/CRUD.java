package uia.dappii.incisoe.crud;
import uia.dappii.incisoe.modelo.Automovil;

import java.sql.*;
import java.util.ArrayList;

public class CRUD {
    public ArrayList<Automovil> select (Connection conn) throws SQLException {
        ArrayList<Automovil> automoviles = new ArrayList<>();
        String sql = "SELECT id_auto, modelo, nombre, color, kilometers, posicion FROM autos";
        Statement stmt = conn.createStatement();
        ResultSet rs = stmt.executeQuery(sql);

        while (rs.next()){
            Automovil automovil = new Automovil();
            automovil.setId(rs.getInt(1));
            automovil.setModelo(rs.getString(2));
            automovil.setNombre(rs.getString(3));
            automovil.setColor(rs.getString(4));
            automovil.setKilometers(rs.getDouble(5));
            automovil.setPosition(rs.getDouble(6));
            automoviles.add(automovil);
            automovil = null;
        }
        stmt.close();
        rs.close();
        return automoviles;
    }

    public int insert(Connection conn, Automovil automovil) throws SQLException {
        String sql = "INSERT INTO autos (id_auto, modelo, nombre, color, kilometers, posicion)" +
                "     VALUES (?,?,?,?,?,?)";
        PreparedStatement pstmt = conn.prepareStatement(sql);

        pstmt.setInt(1, automovil.getId());
        pstmt.setString(2, automovil.getModelo());
        pstmt.setString(3, automovil.getNombre());
        pstmt.setString(4, automovil.getColor());
        pstmt.setDouble(5, automovil.getKilometers());
        pstmt.setDouble(6, automovil.getPosition());
        int i = pstmt.executeUpdate();
        pstmt.close();
        System.out.println("\n Se ha insertado el registro...");
        return 0;
    }
}
