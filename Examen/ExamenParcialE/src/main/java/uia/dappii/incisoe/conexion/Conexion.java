package uia.dappii.incisoe.conexion;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class Conexion {
    Connection conn2;

    public Connection conectar() throws SQLException {
        conn2 = DriverManager.getConnection("jdbc:mysql://localhost:3306/examen_db?useUnicode=true&useJDBCCompliantTimezoneShift=true&useLegacyDatetimeCode=false&serverTimezone=UTC", "root", "M@270220008894Vp");
        if (conn2 != null) {
            return conn2;
        } else {
            return null;
        }
    }

    public void desconectar(Connection conn) throws SQLException{
        conn2.close();
    }
}
