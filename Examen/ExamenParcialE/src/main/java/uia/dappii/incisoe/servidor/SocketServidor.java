package uia.dappii.incisoe.servidor;
import uia.dappii.incisoe.conexion.Conexion;
import uia.dappii.incisoe.crud.CRUD;
import uia.dappii.incisoe.modelo.Automovil;
import uia.dappii.incisoe.modelo.GestorAutos;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;
import java.sql.Connection;
import java.sql.SQLException;

public class SocketServidor {
    public static void main(String[] args) throws IOException, SQLException {
        int puerto = 8000;
        Conexion conexion = new Conexion();
        Connection conn = conexion.conectar();
        GestorAutos gestorAutos = new GestorAutos();
        Automovil automovil = new Automovil();
        CRUD crud = new CRUD();

        ServerSocket miServidor = new ServerSocket(puerto);
        Socket miCliente;

        System.out.println("Esperando Conexion...");

        miCliente = miServidor.accept();

        System.out.println("Recibi Conexion");

        BufferedReader lectorSocket;
        PrintWriter out;

        InputStreamReader in = new InputStreamReader(miCliente.getInputStream());
        lectorSocket = new BufferedReader(in);

        out = new PrintWriter(miCliente.getOutputStream(), true);

        String mensajeRecibido;

        System.out.println("Esperando mensaje....");

        mensajeRecibido = lectorSocket.readLine();

        switch (mensajeRecibido) {
            case "POST":
                System.out.println("\nEstoy en el POST");

                mensajeRecibido = lectorSocket.readLine();
                automovil.fromJson(mensajeRecibido);
                crud.insert(conn, automovil);
                break;
            default:
                System.out.println("Opción no valida.");
                break;
        }
        System.out.println("Opcion: " + mensajeRecibido);
        conexion.desconectar(conn);

    }
}
