package uia.dappii.incisoe.modelo;

import java.util.ArrayList;

public class GestorAutos {
    private ArrayList<Automovil> automoviles = new ArrayList<>();

    public GestorAutos(){}

    public ArrayList<Automovil> getAutomoviles() {
        return automoviles;
    }

    public void setAutomoviles(ArrayList<Automovil> automoviles) {
        this.automoviles = automoviles;
    }
}
