package uia.dappii.incisoe.cliente;

import uia.dappii.incisoe.modelo.Automovil;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;

public class SocketCliente {
    public static void main(String[] args) throws IOException {
        String host = "127.0.0.1";
        int puerto = 8000, op;
        Integer id;
        String modelo, nombre, color;
        Double kilometers, posicion;

        System.out.println("Conectando al Servidor...");
        Socket cliente = new Socket(host, puerto);

        System.out.println("Me conecte al servidor!!!");
        BufferedReader lectorSocket;
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        PrintWriter out;

        InputStreamReader in = new InputStreamReader(cliente.getInputStream());
        lectorSocket = new BufferedReader(in);
        out = new PrintWriter(cliente.getOutputStream(), true);

        String mensajeTransmitido;

        do {
            op = menu();
            switch (op) {
                case 1:
                    mensajeTransmitido = "POST";
                    out.println(mensajeTransmitido);
                    System.out.println("Enviando mensaje...");


                    System.out.print("\nDigita el id: ");
                    id = Integer.valueOf(br.readLine());

                    System.out.print("\nDigita el modelo: ");
                    modelo = br.readLine();

                    System.out.print("\nDigita el nombre: ");
                    nombre = br.readLine();

                    System.out.print("\nDigita el color: ");
                    color = br.readLine();

                    System.out.print("\nDigita los kilometros: ");
                    kilometers = Double.valueOf(br.readLine());

                    System.out.print("\nDigita la posición: ");
                    posicion = Double.valueOf(br.readLine());

                    Automovil automovil = new Automovil(id, modelo, nombre, color, kilometers, posicion);
                    out.println(automovil.toJson());
                    break;
                case 2:
                    opcionSalir();
                    break;
                default:
                    opcionDefault();
                    break;
            }
        } while (op != 2);
    }

    public static int menu() throws IOException {
        int opcion;
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        System.out.println("\n1.- POST");
        System.out.println("2.- Salir");
        System.out.print("\nIndique su opcion: ");
        opcion = Integer.parseInt(br.readLine());
        return opcion;
    }

    public static void opcionSalir(){
        System.out.println("Hasta luego!");
    }

    public static void opcionDefault(){
        System.out.println("Por favor, elije una opción valida...");
    }

}
