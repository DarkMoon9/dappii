package uia.dappii.incisoe.modelo;

import java.util.Objects;

public class Automovil {
    private Integer id;
    private String modelo;
    private String nombre;
    private String color;
    private Double kilometers;
    private Double position;

    public Automovil(Integer id, String modelo, String nombre, String color, Double kilometers, Double position) {
        this.id = id;
        this.modelo = modelo;
        this.nombre = nombre;
        this.color = color;
        this.kilometers = kilometers;
        this.position = position;
    }

    public Automovil() {
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getModelo() {
        return modelo;
    }

    public void setModelo(String modelo) {
        this.modelo = modelo;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public Double getKilometers() {
        return kilometers;
    }

    public void setKilometers(Double kilometers) {
        this.kilometers = kilometers;
    }

    public Double getPosition() {
        return position;
    }

    public void setPosition(Double position) {
        this.position = position;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Automovil automovil = (Automovil) o;
        return Objects.equals(id, automovil.id) && Objects.equals(modelo, automovil.modelo) && Objects.equals(nombre, automovil.nombre) && Objects.equals(color, automovil.color) && Objects.equals(kilometers, automovil.kilometers) && Objects.equals(position, automovil.position);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, modelo, nombre, color, kilometers, position);
    }

    @Override
    public String toString() {
        return "Automovil{" +
                "id=" + id +
                ", modelo='" + modelo + '\'' +
                ", nombre='" + nombre + '\'' +
                ", color='" + color + '\'' +
                ", kilometers=" + kilometers +
                ", position=" + position +
                '}';
    }

    public String toJson() {
        String cadenaJson="{";

        cadenaJson+="\"id\":\""+ id +"\",";
        cadenaJson+="\"modelo\":" + modelo +",";
        cadenaJson+="\"nombre\":\""+ nombre +"\",";
        cadenaJson+="\"color\":\""+ color +"\",";
        cadenaJson+="\"kilometers\":\""+ kilometers +"\",";
        cadenaJson+="\"position\":" + position;

        cadenaJson+="}";
        return cadenaJson;
    }

    public void fromJson(String cadenaJson) {
        cadenaJson = cadenaJson.replace("\"", "");
        cadenaJson = cadenaJson.replace("{", "");
        cadenaJson = cadenaJson.replace("}", "");

        String[] valores = cadenaJson.split(",");

        int inicio = 0, fin = 0;
        String dato = "", campo = "";
        for (String contenido : valores) {
            inicio = contenido.indexOf(":") + 1;
            fin = contenido.indexOf(":");

            dato = contenido.substring(inicio);
            campo = contenido.substring(0, fin);

            switch (campo) {
                case "id":
                    id = Integer.valueOf(dato);
                    break;
                case "modelo":
                    modelo = dato;
                    break;
                case "nombre":
                    nombre = dato;
                    break;
                case "color":
                    color = dato;
                    break;
                case "kilometers":
                    kilometers = Double.valueOf(dato);
                    break;
                case "position":
                    position = Double.valueOf(dato);
                    break;
            }

            //System.out.println(dato);
        }
    }
}
