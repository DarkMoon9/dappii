package uia.dappii.incisod.servidor;
import uia.dappii.incisod.conexion.Conexion;
import uia.dappii.incisod.crud.CRUD;
import uia.dappii.incisod.modelo.GestorAutos;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;
import java.sql.Connection;
import java.sql.SQLException;

public class SocketServidor {
    public static void main(String[] args) throws IOException, SQLException {
        int puerto = 8000;
        Conexion conexion = new Conexion();
        Connection conn = conexion.conectar();
        GestorAutos gestorAutos = new GestorAutos();
        CRUD crud = new CRUD();

        ServerSocket miServidor = new ServerSocket(puerto);
        Socket miCliente;

        System.out.println("Esperando Conexion...");

        miCliente = miServidor.accept();

        System.out.println("Recibi Conexion");

        BufferedReader lectorSocket;
        PrintWriter out;

        InputStreamReader in = new InputStreamReader(miCliente.getInputStream());
        lectorSocket = new BufferedReader(in);

        out = new PrintWriter(miCliente.getOutputStream(), true);

        String mensajeRecibido;

        System.out.println("Esperando mensaje....");

        mensajeRecibido = lectorSocket.readLine();

        switch (mensajeRecibido) {
            case "GET":
                System.out.println("\nEstoy en el GET");
                out.println("\nObteniendo información de la base de datos...");
                gestorAutos.setAutomoviles(crud.select(conn));
                System.out.println(GestorAutos.toXmlList(gestorAutos.getAutomoviles()));
                out.println(GestorAutos.toXmlList(gestorAutos.getAutomoviles()));
                break;
            default:
                System.out.println("Opción no valida.");
                break;
        }
        System.out.println("Opcion: " + mensajeRecibido);
        conexion.desconectar(conn);

    }
}
