package uia.dappii.incisod.crud;
import uia.dappii.incisod.modelo.Automovil;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

public class CRUD {
    public ArrayList<Automovil> select (Connection conn) throws SQLException {
        ArrayList<Automovil> automoviles = new ArrayList<>();
        String sql = "SELECT id_auto, modelo, nombre, color, kilometers, posicion FROM autos";
        Statement stmt = conn.createStatement();
        ResultSet rs = stmt.executeQuery(sql);

        while (rs.next()){
            Automovil automovil = new Automovil();
            automovil.setId(rs.getInt(1));
            automovil.setModelo(rs.getString(2));
            automovil.setNombre(rs.getString(3));
            automovil.setColor(rs.getString(4));
            automovil.setKilometers(rs.getDouble(5));
            automovil.setPosition(rs.getDouble(6));
            automoviles.add(automovil);
            automovil = null;
        }
        stmt.close();
        rs.close();
        return automoviles;
    }
}
