package uia.dappii.incisod.modelo;
import java.util.Objects;

public class Automovil {
    private Integer id;
    private String modelo;
    private String nombre;
    private String color;
    private Double kilometers;
    private Double position;

    public Automovil(Integer id, String modelo, String nombre, String color, Double kilometers, Double position) {
        this.id = id;
        this.modelo = modelo;
        this.nombre = nombre;
        this.color = color;
        this.kilometers = kilometers;
        this.position = position;
    }

    public Automovil() {
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getModelo() {
        return modelo;
    }

    public void setModelo(String modelo) {
        this.modelo = modelo;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public Double getKilometers() {
        return kilometers;
    }

    public void setKilometers(Double kilometers) {
        this.kilometers = kilometers;
    }

    public Double getPosition() {
        return position;
    }

    public void setPosition(Double position) {
        this.position = position;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Automovil automovil = (Automovil) o;
        return Objects.equals(id, automovil.id) && Objects.equals(modelo, automovil.modelo) && Objects.equals(nombre, automovil.nombre) && Objects.equals(color, automovil.color) && Objects.equals(kilometers, automovil.kilometers) && Objects.equals(position, automovil.position);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, modelo, nombre, color, kilometers, position);
    }

    @Override
    public String toString() {
        return "Automovil{" +
                "id=" + id +
                ", modelo='" + modelo + '\'' +
                ", nombre='" + nombre + '\'' +
                ", color='" + color + '\'' +
                ", kilometers=" + kilometers +
                ", position=" + position +
                '}';
    }

    public String toXml() {
        String cadenaXml = "<Automovil>";

        cadenaXml += "<id>" + id +"</id>";
        cadenaXml += "<modelo>" + modelo +"</modelo>";
        cadenaXml += "<nombre>" + nombre +"</nombre>";
        cadenaXml += "<color>" + color +"</color>";
        cadenaXml += "<kilometers>" + kilometers +"</kilometers>";
        cadenaXml += "<position>" + color +"</position>";

        cadenaXml += "</Automovil>";

        return cadenaXml;
    }



}
