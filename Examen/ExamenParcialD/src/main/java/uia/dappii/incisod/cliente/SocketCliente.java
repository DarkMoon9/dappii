package uia.dappii.incisod.cliente;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;

public class SocketCliente {
    public static void main(String[] args) throws IOException {
        String host = "127.0.0.1";
        int puerto = 8000, op;


        System.out.println("Conectando al Servidor...");
        Socket cliente = new Socket(host, puerto);

        System.out.println("Me conecte al servidor!!!");
        BufferedReader lectorSocket;
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        PrintWriter out;

        InputStreamReader in = new InputStreamReader(cliente.getInputStream());
        lectorSocket = new BufferedReader(in);
        out = new PrintWriter(cliente.getOutputStream(), true);

        String mensajeTransmitido, mensajeRecibido;

        do {
            op = menu();
            switch (op) {
                case 1:
                    mensajeTransmitido = "GET";
                    out.println(mensajeTransmitido);
                    System.out.println("Enviando mensaje...");

                    mensajeRecibido = lectorSocket.readLine();
                    System.out.println(mensajeRecibido);

                    mensajeRecibido = lectorSocket.readLine();
                    System.out.println(mensajeRecibido);

                    mensajeRecibido = lectorSocket.readLine();
                    System.out.println(mensajeRecibido);
                    break;
                case 2:
                    opcionSalir();
                    break;
                default:
                    opcionDefault();
                    break;
            }
        } while (op != 2);
    }

    public static int menu() throws IOException {
        int opcion;
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        System.out.println("\n1.- GET");
        System.out.println("2.- Salir");
        System.out.print("\nIndique su opcion: ");
        opcion = Integer.parseInt(br.readLine());
        return opcion;
    }

    public static void opcionSalir(){
        System.out.println("Hasta luego!");
    }

    public static void opcionDefault(){
        System.out.println("Por favor, elije una opción valida...");
    }
}
