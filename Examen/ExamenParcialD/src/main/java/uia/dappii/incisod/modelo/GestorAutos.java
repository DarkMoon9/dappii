package uia.dappii.incisod.modelo;
import java.util.ArrayList;

public class GestorAutos {
    private ArrayList<Automovil> automoviles = new ArrayList<>();

    public GestorAutos(){}

    public ArrayList<Automovil> getAutomoviles() {
        return automoviles;
    }

    public void setAutomoviles(ArrayList<Automovil> automoviles) {
        this.automoviles = automoviles;
    }

    public static String toXmlList(ArrayList<Automovil> automoviles) {
        StringBuilder cadenaXml = new StringBuilder("<Automoviles>");

        for (Automovil automovil: automoviles) {
            cadenaXml.append(automovil.toXml());
        }

        cadenaXml.append("</Automoviles>");
        return cadenaXml.toString();
    }
}
