package uia.dappii.practica13.controller;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import uia.dappii.practica13.model.Persona;
import uia.dappii.practica13.service.IPersonaService;

@Controller
@RequestMapping({"/index", "", "/"})
public class PersonaController {
    @Autowired
    private IPersonaService personaService;

    @GetMapping({"/"})
    public String homeView(Model model){
        model.addAttribute("listaPersonas", personaService.getAllPersonas());
        return "index";
    }

    @PostMapping("/guardarPersona")
    public String guardarPersonaView(@ModelAttribute("persona") Persona persona){
        personaService.guardarPersona(persona);
        return "redirect:/index/";
    }

    @GetMapping("/nuevaPersona")
    public String nuevaPersonaView(Model model){
        Persona persona = new Persona();
        model.addAttribute("persona", persona);
        return "/nueva-persona";
    }

    @GetMapping("/actualizarPersona/{id}")
    public String actualizarPersona(@PathVariable(value = "id") Long id, Model model){
        Persona persona = personaService.getPersonaById(id);
        model.addAttribute("persona", persona);
        return "actualizar-persona";
    }

    @GetMapping("/borrarPersona/{id}")
    public String borrarPersona(@PathVariable(value = "id") Long id){
        this.personaService.borrarPersonaById(id);
        return "redirect:/index/";
    }
}
