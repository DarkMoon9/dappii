package uia.dappii.practica13.repository;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import uia.dappii.practica13.model.Persona;

@Repository
public interface IPersonaRepository extends JpaRepository<Persona, Long> {

}
