package uia.dappii.practica13.service;
import uia.dappii.practica13.model.Persona;
import java.util.List;

public interface IPersonaService {
    List<Persona> getAllPersonas();
    void guardarPersona(Persona persona);
    Persona getPersonaById(Long id);
    void borrarPersonaById(Long id);
}
