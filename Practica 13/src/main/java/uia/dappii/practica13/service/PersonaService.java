package uia.dappii.practica13.service;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import uia.dappii.practica13.model.Persona;
import uia.dappii.practica13.repository.IPersonaRepository;
import java.util.List;
import java.util.Optional;

@Service
public class PersonaService implements IPersonaService {

    @Autowired
    private IPersonaRepository personaRepository;

    @Override
    public List<Persona> getAllPersonas() {
        return personaRepository.findAll();
    }

    @Override
    public void guardarPersona(Persona persona) {
        this.personaRepository.save(persona);
    }

    @Override
    public Persona getPersonaById(Long id) {
        Optional<Persona> optional =personaRepository.findById(id);
        Persona persona = null;
        if(optional.isPresent()){
            persona = optional.get();
        } else {
            throw new RuntimeException("La persona con el id: " + id + " no ha sido encontrada.");
        }
        return persona;
    }

    @Override
    public void borrarPersonaById(Long id) {
        this.personaRepository.deleteById(id);
    }
}
